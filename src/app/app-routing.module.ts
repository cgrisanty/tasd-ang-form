import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { SlidesComponent } from './slides/slides.component';

const routes: Routes = [
  {path: 'contactus', component: ContactComponent},
  {path: 'slides', component: SlidesComponent},
  {path: '', redirectTo: 'contactus', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
