import { Component, OnInit } from '@angular/core';
import {SlidesService} from '../services/slides.service';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss']
})
export class SlidesComponent implements OnInit {

  slides: any;

  constructor(private slidesService: SlidesService) { }

  ngOnInit() {
    this.slidesService.getSlides()
      .subscribe(slides => {this.slides = slides.slideshow; console.log(slides)},
        err => console.log(err.status, err.message));

    console.log(this.slides);
  }

}
