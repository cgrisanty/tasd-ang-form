import { Component, OnInit, ViewChild } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms'
import {FeedbackService} from '../services/feedback.service';
import {Feedback} from '../shared/feedback';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  feedback: Feedback;
  @ViewChild('fform') feedbackFormDirective;

  constructor(private fb: FormBuilder, private feedbackService: FeedbackService) { }

  ngOnInit() {
    this.feedbackForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required]
    });
  }

  onSubmit(){
    console.log(this.feedbackForm.value);
    this.feedback = this.feedbackForm.value;
    this.feedbackService.postFeedback(this.feedback)
      .subscribe(resp => { //response
        console.log(resp);
        this.feedback = null;
      }, err => console.log(err.status, err.message));
    
    this.feedbackForm.reset();
    this.feedbackFormDirective.resetForm();
  }

}
