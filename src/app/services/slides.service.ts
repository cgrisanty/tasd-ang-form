import { Injectable } from '@angular/core';

import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {baseURL} from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class SlidesService {

  constructor(private http: HttpClient) { }

  getSlides(): Observable<any>{
    return this.http.get<any>(baseURL+ '/json')
      .pipe(catchError(this.handleError));
  }

  handleError(error: HttpErrorResponse | any){    
    let errMsg: string;

    if(error.error instanceof ErrorEvent){
      errMsg = error.error.message;
    } else{
      errMsg = `${error.status} - ${error.statusText || ''} ${error.message}`;
    }

    return throwError(errMsg);
  }
}
