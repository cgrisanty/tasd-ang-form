import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Feedback} from '../shared/feedback';
import {catchError} from 'rxjs/operators';

import {baseURL} from '../shared/baseurl';
import {HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient) { }

  postFeedback(feedback: Feedback): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      })
    };
    return this.http.post<any>(baseURL+ '/anything', feedback, httpOptions)
      .pipe(catchError(this.handleError))
  }

  handleError(error: HttpErrorResponse | any) {
    let errMsg: string;

    if(error.error instanceof ErrorEvent){
      errMsg = error.error.message;
    } else{
      errMsg = `${error.status} - ${error.statusText || ''} ${error.message}`;
    }

    return throwError(errMsg);
  }
}
